var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
var compass = require('gulp-compass');
var watch = require('gulp-watch');

gulp.task('compress', function (cb) {
  pump([
    gulp.src(['js/*.js', '!./gulpfile.js']),
    uglify(),
    gulp.dest('dist/min')
  ],
    cb
  );
});

gulp.task('compass', function () {
  gulp.src('./sass/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: 'dist',
      sass: 'sass'
    }))
    .pipe(gulp.dest('dist/min'));
});

gulp.task('watch', function () {
  gulp.watch(['js/*.js', '!./gulpfile.js'], ['compress']);
  gulp.watch('./sass/*.scss', ['compass']);
});

gulp.task('default', ['watch']);